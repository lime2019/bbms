## 1. 创建项目

1.1 安装express手脚架

```bash
npm install express-generator -g --registry=https://registry.npm.taobao.org
```

1.2 生成项目

```bash
express bbms_server
```

1.3 安装依赖

```bash
# 进入项目文件夹
cd bbms_server
# 安装依赖
npm install
# 启动项目
npm start
```

1.4 项目中所用插件
```bash
# 阿里云官方镜像地址，加速下载
--registry=https://registry.npm.taobao.org
# nodemon：可以跟随你修改代码后自动重启服务；
npm i nodemon --save-dev
# cross-env：提供一个设置环境变量的scripts，让你能够以unix方式设置环境变量，然后在windows上也能兼容运行；
 npm i cross-env --save-dev
# 安装mysql插件、防止sql注入
npm i mysql --save
# 安装插件，防止xss攻击
npm i xss --save
# 生成session的功能
npm i express-session --save
# 安装redis及connect-redis
npm i redis connect-redis --save
# 安装PM2
npm
```

## 2. 项目目录介绍

2.1 `/bin`文件夹中`www.js`文件

提供http服务；

2.2 `/routers`文件夹

存放页面路由文件；

2.3 `app.js`文件

项目入口文件；

2.4 `.gitignore`文件

git上传时忽略的文件；

2.5 `package.json`文件

配置文件；

2.6 `db`文件夹

数据库相关配置