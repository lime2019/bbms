// 错误页的处理
const createError = require('http-errors');
// 引入express
const express = require('express');
// nodejs提供的路径工具
// const path = require('path');
// 解析cookie
const cookieParser = require('cookie-parser');
// 记录日志
const logger = require('morgan');
// 引用session
// const session = require('express-session')
// 引用redis
// const RedosStore = require('connect-redis')(session)

// 引入路由
const loginRouter = require('./routes/login')
const usersRouter = require('./routes/users')
const booksRouter = require('./routes/books')
const cardsRouter = require('./routes/cards')
const recordsRouter = require('./routes/records')

// 初始化app，本次http请求的实例
const app = express();


// 记录日志
app.use(logger('dev'));

// console.log(process.env.PORT)

/*
处理POST请求的数据
如果后面需要使用POST数据；
可以直接用req.body获取，不用写获取的方法；
 */
app.use(express.json());
// 对获取的非json格式的数据进行处理；
app.use(express.urlencoded({ extended: false }));


// 解析cookie
// app.use(cookieParser());

// const redisClient = require('./db/redis')
// const sessionStore = new RedosStore({
//   client:redisClient
// })

// 解析session
// app.use(session({
//   // 类似于密匙
//   secret:'lime2019@QQ.com',
//   cookie:{
//     // 默认配置
//     path:'/',
//     httpOnly: true,
//     // 过期时间
//     maxAge:24*60*60*1000
//   },
//   store:sessionStore
// }))

/*
注册路由，分别为：
登录接口；
系统用户信息接口；
图书信息接口；
借阅证信息接口；
借阅记录信息接口；
 */
app.use('/api/login',loginRouter)
app.use('/api/users',usersRouter)
app.use('/api/books',booksRouter)
app.use('/api/cards',cardsRouter)
app.use('/api/records',recordsRouter)


// 访问路径不对，处理404页面
app.use(function(req, res, next) {
  next(createError(404));
});

// 处理错误程序，服务器错误，抛出500的错误
app.use(function(err, req, res, next) {
  // 设置局部变量，仅在开发环境中提供错误；
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'dev' ? err : {};
  // 生成错误页
  res.status(err.status || 500);
  // res.render('error');
});

module.exports = app;
