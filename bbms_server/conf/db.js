// 创建一个常量，读取当前运行环境；
const env =  process.env.NODE_ENV

// 创建两个变量，分别保存MySQL配置和Redis配置
let MYSQL_CONF
let REDIS_CONF

// 开发环境下运行
if(env === 'dev'){
    MYSQL_CONF = {
        host:'localhost',
        user:'root',
        password:'123456',
        port:'3306',
        database:'bbms'
    }
    //
    REDIS_CONF = {
        port:6379,
        host:'127.0.0.1',
        password:{auth_pass:'123456'}
    }
}

// 生产环境下运行
if(env === 'production'){
    MYSQL_CONF = {
        host:'127.0.0.1',
        user:'root',
        password:'dzLEVCuo3YS^syC$',
        port:'3306',
        database:'bbms'
    }
    REDIS_CONF = {
        port:6379,
        host:'127.0.0.1',
        password:{auth_pass:'dzLEVCuo3YS^syC$'}
    }
}

// 导出MySQL和Redis配置信息
module.exports ={
    MYSQL_CONF,
    REDIS_CONF
}