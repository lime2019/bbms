// 引入运行sql的插件和防止sql 注入的插件
const { exec, escape } = require('../db/mysql')

// 图书列表
const listBook = (role) =>{
    let sql = ``
    switch (role) {
        case 2:{
            sql = `select * from book;`
            break;
        }
        case 1:{
            sql = `select book_ibsn_id,book_title,book_author,book_gist,book_img,book_page,book_price,book_publisher,book_pubdate,book_address,book_now_number,book_number,book_create_time from book where book_is_deleted=0;`
            break;
        }
        default:
            sql = `select book_ibsn_id,book_title,book_author,book_gist,book_img,book_page,book_price,book_publisher,book_pubdate,book_address,book_now_number,book_number from book where book_is_deleted=0;`
    }
    console.log(sql);
    return exec(sql).then(data=>{
        // console.log(data);
        data.errno = 0
        return data
    }).catch(err => {
        data.errno = 1
        return err
    })
}

// 查询图书
const searchBook = (role,type,input) => {
    // 防止sql注入
    input = escape(input)
    let sql = ``
    switch (role) {
        case 2:{
            sql = `select * from book where ${type}=${input};`
            break;
        }
        case 1:{
            sql = `select book_ibsn_id,book_title,book_author,book_gist,book_img,book_page,book_price,book_publisher,book_pubdate,book_address,book_now_number,book_number,book_create_time from book where ${type}=${input} and book_is_deleted=0;`
            break;
        }
        default:
            sql = `select book_ibsn_id,book_title,book_author,book_gist,book_img,book_page,book_price,book_publisher,book_pubdate,book_address,book_now_number,book_number from book where ${type}=${input} and book_is_deleted=0;`
    }
    console.log(sql);
    return exec(sql).then(data=>{
        // console.log(data)
        if(data.length>0){
            data.errno = 0
        }else{
            data.errno = 1
        }
        return data
    }).catch(err => {
        data.errno = -1
        return err
    })
}

// 新增图书
const newBook = (bookData) => {
    const sql = `insert into book (book_ibsn_id,book_title,book_author,book_gist,book_img,book_page,book_price,book_publisher,book_pubdate,book_address,book_now_number,book_number,book_create_time,book_update_time) values('${bookData.book_ibsn_id}', '${bookData.book_title}', '${bookData.book_author}', '${bookData.book_gist}', '${bookData.book_img}', ${bookData.book_page}, ${bookData.book_price}, '${bookData.book_publisher}', '${bookData.book_pubdate}','${bookData.book_address}', ${bookData.book_now_number}, ${bookData.book_number}, '${bookData.book_create_time}', '${bookData.book_update_time}');`
    console.log(sql);
    return exec(sql).then(data =>{
        /*
        data返回值
        OkPacket {
          fieldCount: 0,
          affectedRows: 1,
          insertId: 0,
          serverStatus: 2,
          warningCount: 0,
          message: '',
          protocol41: true,
          changedRows: 0
        }
        */
        // console.log(data);
        data.errno =0
        return data
    }).catch(err => {
        err.errno = 1
        return err
    })
}

// 修改图书信息
const updateBook = (ibsn,type,input) => {
    const sql = `update book set ${type}=${input} where book_ibsn_id=${ibsn};`
    console.log(sql);
    return exec(sql).then(data => {
        // console.log(data)
        data.errno = 0
        return data
    }).catch(err=>{
        err.errno = 1
        return err
    })
}

// 删除图书信息
const deleteBook = (ibsn) => {
    const sql = `delete from book where book_ibsn_id=${ibsn};`
    console.log(sql);
    return exec(sql).then(data => {
        /*
        OkPacket {
          fieldCount: 0,
          affectedRows: 1,
          insertId: 0,
          serverStatus: 34,
          warningCount: 0,
          message: '',
          protocol41: true,
          changedRows: 0
        }
        */
        // console.log(data);
        data.errno = 0
        return data
    }).catch(err => {
        err.errno =1
        return err
    })
}

module.exports = {
    searchBook,
    listBook,
    newBook,
    updateBook,
    deleteBook
}