// 引入运行sql的插件和防止sql 注入的插件
const { exec, escape } = require('../db/mysql')
// 引入加密函数
const { genPassword } = require('../utils/cryp')

const login = (username,password) => {
    // 防sql注入
    username = escape(username)
    // 生成加密密码
    password = genPassword(password)
    password = escape(password)
    // 生成sql语句
    const sql = `select user_id,user_email,user_role from user where user_id=${username} and user_password=${password} and user_is_deleted=0;`
    console.log(sql)
    return exec(sql).then(data => {
        console.log(data);
        return data[0] || {}
    })
}

module.exports = {
    login
}