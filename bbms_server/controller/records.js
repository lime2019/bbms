// 引入运行sql的插件和防止sql 注入的插件
const { exec, escape } = require('../db/mysql')

// 借阅记录列表
const listRecord = (role) => {
    let sql = ``
    if(role>1){
        sql = `select * from record;`
    }else{
        sql = `select record_id,record_book_ibsn,record_book_title,record_book_author,record_book_publisher,record_book_price,record_book_address,record_library_card,record_student_name,record_student_phone,record_expire_time,record_create_time from record where record_is_delete=0;`
    }
    console.log(sql);
    return exec(sql).then(data => {
        console.log(data)
        return data
    })
}

// 查询借阅记录
const searchRecord = (role,id,type,input) => {
    input =escape(input)
    let sql = ``
    console.log(type.length)
    if(role >1){
        if(type.length === 4 ){
            sql = `select * from record where record_expire_time<${input};`
        }else{
            sql = `select * from record where ${type}=${input};`
        }
    }else{
        if(role >0){
            if(type.length === 4 ){
                sql = `select record_id,record_book_ibsn,record_book_title,record_book_author,record_book_publisher,record_book_price,record_book_address,record_library_card,record_student_name,record_student_phone,record_expire_time,record_create_time,record_return from record where record_expire_time<${input} and record_is_delete=0;`
            }else{
                sql = `select record_id,record_book_ibsn,record_book_title,record_book_author,record_book_publisher,record_book_price,record_book_address,record_library_card,record_student_name,record_student_phone,record_expire_time,record_create_time,record_return from record where ${type}=${input} and record_is_delete=0;`
            }
        }else{
            sql = `select record_id,record_book_ibsn,record_book_title,record_book_author,record_book_publisher,record_book_price,record_book_address,record_library_card,record_student_name,record_student_phone,record_expire_time,record_create_time,record_return from record where record_library_card=${id} and ${type}=${input} and record_is_delete=0;`
        }
    }
    console.log(sql)
    return exec(sql).then(data => {
        console.log(data)
        return data
    })
}

// 新增借阅记录
const newRecord = (row,card,time,expire_time) => {
    const sql = `insert into record (record_book_ibsn,record_book_title,record_book_author,record_book_publisher,record_book_price,record_book_address,record_library_card,record_student_name,record_student_phone,record_create_time,record_expire_time,record_update_time) values('${row.book_ibsn_id}','${row.book_title}','${row.book_author}','${row.book_publisher}',${row.book_price},'${row.book_address}',${card.library_card_id},'${card.student_name}','${card.student_phone}','${time}','${expire_time}','${time}')`
    console.log(sql)
    return exec(sql).then(data => {
        data.errno =0
        return data
    }).catch(err => {
        err.errno = 1
        return err
    })
}

// 更新借阅记录
const updateRecord = (id,type,input) => {
    input = escape(input)
    const sql = `update record set ${type}=${input} where record_id=${id};`
    console.log(sql);
    return exec(sql).then(data => {
        console.log(data)
        data.errno = 0
        return data
    }).catch(err => {
        err.errno = 1
        return err
    })
}

// 删除借阅记录
const deleteRecord = (id) => {
    id = escape(id)
    const sql =`delete from record where record_id=${id};`
    console.log(sql);
    return exec(sql).then(data => {
        console.log(data)
        data.errno = 0
        return data
    }).catch(err => {
        err.errno = 1
        return err
    })
}

module.exports = {
    listRecord,
    searchRecord,
    newRecord,
    updateRecord,
    deleteRecord
}