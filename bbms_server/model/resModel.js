// 响应模板
class BaseModel {
    constructor(msg, code) {
        if (typeof msg === 'number') {
            this.code = msg
            msg = null
            code = null
        }
        if (msg) {
            this.msg = msg
        }
        if (code) {
            this.code = code
        }
    }
}

class SuccessModel extends BaseModel {
    constructor(msg, code) {
        super(msg, code)
    }
}

class ErrorModel extends BaseModel {
    constructor(msg, code) {
        super(msg, code)
    }
}

module.exports = {
    SuccessModel,
    ErrorModel
}