const express = require('express');
const axios = require('axios')
const router = express.Router();
const {searchBook,
    listBook,
    newBook,
    updateBook,
    deleteBook} = require('../controller/books')
const {SuccessModel,ErrorModel} = require('../model/resModel')

// 获取图书列表
router.get('/list',(req,res,next) => {
    // 获取权限信息
    const role = parseInt(req.query.role)
    const result = listBook(role)
    return result.then(data =>{
        if(data.errno === 0){
            res.json(new SuccessModel(data,200))
            return
        }
        res.json(new ErrorModel(500))
    })
    next()
})

// 查询图书
router.get('/search',(req,res,next) => {
    // 获取权限信息
    const role = parseInt(req.query.role)
    // 从请求参数中获取结果
    const result = searchBook(role,req.query.type,req.query.input)
    return result.then(data => {
        // console.log(data)
        if(data.errno === 0){
            res.json(new SuccessModel(data,200))
        }else{
            if(data.errno === -1){
                res.json(new ErrorModel(500))
            }else{
                res.json(new ErrorModel(404))
            }
        }
    })
    next()
})

// 新增图书，使用聚合数据的API
router.post('/new',(req,res,next)=>{
    const {role,ibsn,address,number} = req.body
    if(role !== 2){
        res.json(new ErrorModel(403))
    }else{
        axios.get('http://feedback.api.juhe.cn/ISBN',{
            params:{
                sub:ibsn,
                key:'8673a8768c00add73c34e85bdb576317'
            }
        }).then(response=>{
            let {title,author,summary,images_large,pages,price,publisher,pubdate} = response.data.result
            const create_time = Date.now()
            const bookData = {
                book_ibsn_id:ibsn,
                book_title:title,
                book_author:author,
                book_gist:summary,
                book_img:images_large,
                book_page:pages,
                book_price:price,
                book_publisher:publisher,
                book_pubdate:pubdate,
                book_address:address,
                book_now_number:number,
                book_number:number,
                book_create_time:create_time,
                book_update_time:create_time
            }
            const result = newBook(bookData)
            return result.then(data =>{
                if(data.errno === 1){
                    res.json(new ErrorModel(422))
                }else{
                    if(data.affectedRows >0){
                        res.json(new SuccessModel(data,200))
                        return
                    }
                    res.json(new ErrorModel(500))
                }
            })
        })
    }
    next()
})

// 修改图书信息
router.post('/update',(req,res,next)=>{
    let {role,ibsn,type,input} = req.body
    role = parseInt(role)
    if(role === 0 && type !== 'book_now_number'){
        res.json(new ErrorModel(403))
    }else{
        const result = updateBook(ibsn,type,input)
        return result.then(data => {
            if(data.errno === 1){
                res.json(new ErrorModel(422))
            }else{
                if(data.affectedRows>0){
                    res.json(new SuccessModel(data,200))
                    return
                }
                res.json(new ErrorModel(500))
            }
        })
    }
    next()
})

// 删除图书
router.post('/delete',(req,res,next)=>{
    if(req.body.role !== 2){
        res.json(new ErrorModel(403))
    }else{
        const result = deleteBook(req.body.ibsn)
        return result.then(data => {
            if(data.errno){
                res.json(new ErrorModel(422))
            }else{
                if(data.affectedRows >0){
                    res.json(new SuccessModel(data,200))
                    return
                }
                res.json(new ErrorModel(500))
            }
        })
    }
    next()
})

// 理论上应该还有个处理404页面的

module.exports = router;
