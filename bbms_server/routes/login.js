const express = require('express');
const router = express.Router();
const {login} = require('../controller/login')
const {SuccessModel,ErrorModel} = require('../model/resModel')

// 登录
router.post('/',function (req,res,next) {
    // 从请求参数中解构获得username和password
    const {username,password} = req.body
    // 进行登录，将数据交给控制层，得到结果
    const result = login(username,password)
    // 将得到的结果发送给前端
    return result.then(data=>{
        if(data.user_id){
            res.json(new SuccessModel(data,200))
            return
        }
        res.json(new ErrorModel(404))
    })
})

module.exports = router;