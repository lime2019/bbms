const express = require('express');
const router = express.Router();
const {listRecord,
    searchRecord,
    newRecord,
    updateRecord,
    deleteRecord} = require('../controller/records')
const {SuccessModel,ErrorModel} = require('../model/resModel')

// 借阅记录列表
router.get('/list',function (req,res,next) {
    // 获取权限信息
    const role = req.query.role
    if(role>0){
        const result = listRecord(role)
        return result.then(data => {
            res.json(new SuccessModel(data,200))
        })
    }else{
        res.json(403)
    }
    next()
})

// 查询借阅记录
router.get('/search',(req,res,next) =>{
    if(req.query.role<0){
        res.json(new ErrorModel(401))
    }else{
        // 获取要查询的信息
        const result = searchRecord(req.query.role,req.query.id,req.query.type,req.query.input)
        return result.then(data => {
            res.json( new SuccessModel(data,200))
        })
    }
    next()
})

// 新增借阅记录,提前做是否存在的校验
router.post('/new',(req,res,next) => {
    const {role,row,card} = req.body
    if(role<0){
        res.json(new ErrorModel(401))
    }else{
        if(role>0){
            res.json(new ErrorModel(403))
        }else{
            const time = Date.now()
            const expire_time = time + 60*24*60*60*1000
            return newRecord(row,card,time, expire_time).then(data => {
                if(data.errno){
                    res.json(new ErrorModel(404))
                }else{
                    if(data.affectedRows >0){
                        res.json(new SuccessModel(data,200))
                        return
                    }
                    res.json(new ErrorModel(500))
                }
            })
        }
    }
    next()
})

// 修改借阅记录
router.post('/update',(req,res,next) => {
    const {role,id,type,input} = req.body
    if(role>=0){
        const result = updateRecord(id,type,input)
        return result.then(data => {
            if(data.errno === 1){
                res.json(new ErrorModel(422))
            }else{
                if(data.affectedRows>0){
                    res.json(new SuccessModel(data,200))
                    return
                }
                res.json(new ErrorModel(500))
            }
        })
    }else{
        res.json(new ErrorModel(401))
    }
    next()
})

// 删除借阅记录
router.post('/delete',(req,res,next) => {
    const {role,id} = req.body
    if(role < 2){
        if(role < 0){
            res.json(new ErrorModel(401))
        }
        res.json(new ErrorModel(403))
    }else{
        const result = deleteRecord(id)
        return result.then(data => {
            if(data.errno){
                res.json(new ErrorModel(422))
            }else{
                if(data.affectedRows>0){
                    res.json(new SuccessModel(data,200))
                    return
                }
                res.json(new ErrorModel(500))
            }
        })
    }
    next()
})

module.exports = router;