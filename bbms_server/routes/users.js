const express = require('express');
// 引入加密函数
const { genPassword } = require('../utils/cryp')
const router = express.Router();
const {searchUser,
    listUser,
    newUser,
    updateUser,
    deleteUser} = require('../controller/users')
const {SuccessModel,ErrorModel} = require('../model/resModel')

//  用户信息列表
router.get('/list',function (req,res,next) {
    // 获取权限信息
    const role = parseInt(req.query.role)
    if(role>0){
        const result = listUser(role)
        return result.then(data => {
            res.json(new SuccessModel(data,200))
        })
    }else{
        res.json(new ErrorModel(403))
    }
    next()
})

// 查询用户信息
router.get('/search', function(req, res, next) {
    let {role} = req.query
    if(role >=0){
        const result = searchUser(role,req.query.type,req.query.input)
        return result.then(data => {
            if(data.errno === 0){
                res.json(new SuccessModel(data,200))
                return
            }
            res.json(new ErrorModel(500))
        })
    }else{
        res.json(new ErrorModel(401))
    }
    next()
})

// 新增用户信息
router.post('/new', function(req, res, next) {
    let {myRole,id,password,email,role} = req.body
    // 生成加密密码
    password = genPassword(password)
    password = escape(password)
    if(myRole>role){
        const time = Date.now()
        const result = newUser(id,password,email,role,time)
        return result.then(data => {
            if(data.errno === 1){
                res.json(new ErrorModel(422))
            }else{
                if(data.affectedRows>0){
                    res.json(new SuccessModel(data,200))
                    return
                }
                res.json(new ErrorModel(500))
            }
        })
    }else{
        res.json(new ErrorModel(403))
    }
    next()
})

// 修改用户信息
router.post('/update', function(req, res, next) {
    let {role,user_id,type,input} = req.body
    if(role <0){
        res.json(new ErrorModel(401))
    }else{
        if(type === 'user_password'){
            input = genPassword(input)
            input = escape(input)
        }
        const result = updateUser(role,user_id,type,input)
        return result.then(data => {
            if(data.errno){
                res.json(new ErrorModel(422))
            }else{
                if(data.affectedRows >0){
                    res.json(new SuccessModel(data,200))
                    return
                }
                res.json(new ErrorModel(500))
            }
        })
    }
    next()
})

router.post('/delete', function(req, res, next) {
    let {role,id} = req.body
    role = parseInt(role)
    if(role !== 2){
        res.json(new ErrorModel(403))
    }else{
        const result = deleteUser(id)
        return result.then(data =>{
            if(data.affectedRows >0){
                res.json(new SuccessModel(data,200))
                return
            }
            res.json(new ErrorModel(500))
        })
    }
    next()
})

module.exports = router;