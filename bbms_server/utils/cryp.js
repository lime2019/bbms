// 此js文件用于加密密码，密码在数据库中以密文保存
const crypto = require('crypto')

// 密匙
const SECRET_KEY = 'Lime2019'

// md5 加密
function md5(content) {
    let md5 = crypto.createHash('md5')
    return md5.update(content).digest('hex')
}

// 加密函数
function genPassword(password) {
    const str = `password=${password}&key=${SECRET_KEY}`
    // 重点是md5加密，字符串中存在密码，密码不同生成的字符串也不同
    return md5(str)
}

// const psd = genPassword('123456')
// console.log(psd);

module.exports = {
    genPassword
}