/*
 Navicat Premium Data Transfer

 Source Server         : 本地MySQL
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : bbms

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 22/07/2020 01:29:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book`  (
  `book_ibsn_id` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '国际图书编号（IBSN）',
  `book_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书名称',
  `book_author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书作者',
  `book_gist` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '图书简介',
  `book_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图书图片地址',
  `book_page` int NULL DEFAULT NULL COMMENT '图书页数',
  `book_price` decimal(6, 2) UNSIGNED NOT NULL COMMENT '图书单价',
  `book_publisher` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书出版社',
  `book_pubdate` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书出版时间',
  `book_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书库存地址',
  `book_now_number` int UNSIGNED NOT NULL COMMENT '图书当前在库数量',
  `book_number` int UNSIGNED NOT NULL COMMENT '图书总数量',
  `book_create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建时间',
  `book_update_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新时间',
  `book_is_deleted` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '逻辑删除状态，0表示未删除，1表示删除',
  PRIMARY KEY (`book_ibsn_id`) USING BTREE,
  INDEX `book_title`(`book_title`) USING BTREE,
  INDEX `book_author`(`book_author`) USING BTREE,
  INDEX `book_img`(`book_img`) USING BTREE,
  INDEX `book_publisher`(`book_publisher`) USING BTREE,
  INDEX `book_price`(`book_price`) USING BTREE,
  INDEX `book_address`(`book_address`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for card
-- ----------------------------
DROP TABLE IF EXISTS `card`;
CREATE TABLE `card`  (
  `library_card_id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '借书证编号',
  `student_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生姓名',
  `student_department` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生系别',
  `student_major` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生专业',
  `student_phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生联系方式',
  `student_borrowed_number` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '当前借书数',
  `student_limit` int UNSIGNED NOT NULL DEFAULT 20 COMMENT '借阅上限',
  `student_account` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学号（系统登录账号）',
  `student_is_arrears` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否存在欠缴金额，0为不欠缴，1为欠缴',
  `student_create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建时间',
  `student_update_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新时间',
  `student_is_delete` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '逻辑删除标识，0表示未删除，1表示删除',
  PRIMARY KEY (`library_card_id`) USING BTREE,
  INDEX `id`(`student_account`) USING BTREE,
  INDEX `student_name`(`student_name`) USING BTREE,
  INDEX `student_phone`(`student_phone`) USING BTREE,
  CONSTRAINT `id` FOREIGN KEY (`student_account`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 100010 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for record
-- ----------------------------
DROP TABLE IF EXISTS `record`;
CREATE TABLE `record`  (
  `record_id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '借书记录唯一单号',
  `record_book_ibsn` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所借阅图书的IBSN',
  `record_book_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所借阅图书的名称',
  `record_book_author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所借阅图书的作者',
  `record_book_publisher` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所借阅图书的出版社',
  `record_book_price` decimal(6, 2) NOT NULL COMMENT '所借阅图书的单价',
  `record_book_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所借阅图书的库存地址',
  `record_library_card` int UNSIGNED NOT NULL COMMENT '借书人的借书证编号',
  `record_student_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '借书人姓名',
  `record_student_phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '借书人手机号',
  `record_create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建时间，即借阅时间',
  `record_expire_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '到期时间',
  `record_update_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新时间',
  `record_return` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '0未未归还，1为已归还',
  `record_is_delete` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '逻辑删除，0表示未删除，1表示删除',
  PRIMARY KEY (`record_id`) USING BTREE,
  INDEX `IBSN`(`record_book_ibsn`) USING BTREE,
  INDEX `Title`(`record_book_title`) USING BTREE,
  INDEX `Author`(`record_book_author`) USING BTREE,
  INDEX `Publisher`(`record_book_publisher`) USING BTREE,
  INDEX `Price`(`record_book_price`) USING BTREE,
  INDEX `Address`(`record_book_address`) USING BTREE,
  INDEX `CradID`(`record_library_card`) USING BTREE,
  INDEX `Name`(`record_student_name`) USING BTREE,
  INDEX `Phone`(`record_student_phone`) USING BTREE,
  CONSTRAINT `Address` FOREIGN KEY (`record_book_address`) REFERENCES `book` (`book_address`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Author` FOREIGN KEY (`record_book_author`) REFERENCES `book` (`book_author`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `CradID` FOREIGN KEY (`record_library_card`) REFERENCES `card` (`library_card_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `IBSN` FOREIGN KEY (`record_book_ibsn`) REFERENCES `book` (`book_ibsn_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Name` FOREIGN KEY (`record_student_name`) REFERENCES `card` (`student_name`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Title` FOREIGN KEY (`record_book_title`) REFERENCES `book` (`book_title`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1000000002 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id(登录账号、学号)',
  `user_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录密码',
  `user_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户邮箱',
  `user_role` int UNSIGNED NOT NULL COMMENT '用户角色（0为学生、1为管理员、2为负责人）',
  `user_create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建时间',
  `user_update_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新时间',
  `user_is_deleted` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '逻辑删除标识，0为未删除，1为删除',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
