/*
 Navicat Premium Data Transfer

 Source Server         : 本地MySQL
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : bbms

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 22/07/2020 01:34:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book`  (
  `book_ibsn_id` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '国际图书编号（IBSN）',
  `book_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书名称',
  `book_author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书作者',
  `book_gist` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '图书简介',
  `book_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图书图片地址',
  `book_page` int NULL DEFAULT NULL COMMENT '图书页数',
  `book_price` decimal(6, 2) UNSIGNED NOT NULL COMMENT '图书单价',
  `book_publisher` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书出版社',
  `book_pubdate` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书出版时间',
  `book_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书库存地址',
  `book_now_number` int UNSIGNED NOT NULL COMMENT '图书当前在库数量',
  `book_number` int UNSIGNED NOT NULL COMMENT '图书总数量',
  `book_create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建时间',
  `book_update_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新时间',
  `book_is_deleted` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '逻辑删除状态，0表示未删除，1表示删除',
  PRIMARY KEY (`book_ibsn_id`) USING BTREE,
  INDEX `book_title`(`book_title`) USING BTREE,
  INDEX `book_author`(`book_author`) USING BTREE,
  INDEX `book_img`(`book_img`) USING BTREE,
  INDEX `book_publisher`(`book_publisher`) USING BTREE,
  INDEX `book_price`(`book_price`) USING BTREE,
  INDEX `book_address`(`book_address`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
