/*
 Navicat Premium Data Transfer

 Source Server         : 本地MySQL
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : bbms

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 22/07/2020 01:29:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for card
-- ----------------------------
DROP TABLE IF EXISTS `card`;
CREATE TABLE `card`  (
  `library_card_id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '借书证编号',
  `student_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生姓名',
  `student_department` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生系别',
  `student_major` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生专业',
  `student_phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生联系方式',
  `student_borrowed_number` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '当前借书数',
  `student_limit` int UNSIGNED NOT NULL DEFAULT 20 COMMENT '借阅上限',
  `student_account` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学号（系统登录账号）',
  `student_is_arrears` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否存在欠缴金额，0为不欠缴，1为欠缴',
  `student_create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建时间',
  `student_update_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新时间',
  `student_is_delete` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '逻辑删除标识，0表示未删除，1表示删除',
  PRIMARY KEY (`library_card_id`) USING BTREE,
  INDEX `id`(`student_account`) USING BTREE,
  INDEX `student_name`(`student_name`) USING BTREE,
  INDEX `student_phone`(`student_phone`) USING BTREE,
  CONSTRAINT `id` FOREIGN KEY (`student_account`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 100010 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
