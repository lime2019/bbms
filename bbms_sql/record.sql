/*
 Navicat Premium Data Transfer

 Source Server         : 本地MySQL
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : bbms

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 22/07/2020 01:29:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for record
-- ----------------------------
DROP TABLE IF EXISTS `record`;
CREATE TABLE `record`  (
  `record_id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '借书记录唯一单号',
  `record_book_ibsn` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所借阅图书的IBSN',
  `record_book_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所借阅图书的名称',
  `record_book_author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所借阅图书的作者',
  `record_book_publisher` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所借阅图书的出版社',
  `record_book_price` decimal(6, 2) NOT NULL COMMENT '所借阅图书的单价',
  `record_book_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所借阅图书的库存地址',
  `record_library_card` int UNSIGNED NOT NULL COMMENT '借书人的借书证编号',
  `record_student_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '借书人姓名',
  `record_student_phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '借书人手机号',
  `record_create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建时间，即借阅时间',
  `record_expire_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '到期时间',
  `record_update_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新时间',
  `record_return` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '0未未归还，1为已归还',
  `record_is_delete` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '逻辑删除，0表示未删除，1表示删除',
  PRIMARY KEY (`record_id`) USING BTREE,
  INDEX `IBSN`(`record_book_ibsn`) USING BTREE,
  INDEX `Title`(`record_book_title`) USING BTREE,
  INDEX `Author`(`record_book_author`) USING BTREE,
  INDEX `Publisher`(`record_book_publisher`) USING BTREE,
  INDEX `Price`(`record_book_price`) USING BTREE,
  INDEX `Address`(`record_book_address`) USING BTREE,
  INDEX `CradID`(`record_library_card`) USING BTREE,
  INDEX `Name`(`record_student_name`) USING BTREE,
  INDEX `Phone`(`record_student_phone`) USING BTREE,
  CONSTRAINT `Address` FOREIGN KEY (`record_book_address`) REFERENCES `book` (`book_address`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Author` FOREIGN KEY (`record_book_author`) REFERENCES `book` (`book_author`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `CradID` FOREIGN KEY (`record_library_card`) REFERENCES `card` (`library_card_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `IBSN` FOREIGN KEY (`record_book_ibsn`) REFERENCES `book` (`book_ibsn_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Name` FOREIGN KEY (`record_student_name`) REFERENCES `card` (`student_name`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Title` FOREIGN KEY (`record_book_title`) REFERENCES `book` (`book_title`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1000000002 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
