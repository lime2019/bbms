---------------
## 学生API实现
---------------

# 图书信息
-----------

-- 图书信息列表
select book_ibsn_id,book_title,book_author,book_gist,book_img,book_page,book_price,book_publisher,book_pubdate,book_address,book_now_number,book_number from book where book_is_deleted=0;

-- 查询图书信息
select book_ibsn_id,book_title,book_author,book_gist,book_img,book_page,book_price,book_publisher,book_pubdate,book_address,book_now_number,book_number from book where book_ibsn_id='9787111407010' and book_is_deleted=0;

-- 修改图书信息（借阅时，减少一本在库图书，归还时，增加一本在库图书）
update book set book_now_number='1' where book_ibsn_id=9787115396860;
update book set book_now_number='2' where book_ibsn_id=9787115396860;


# 借阅证信息（仅有学生本人的）
-----------

-- 查询借阅证信息
select library_card_id,student_name,student_department,student_major,student_phone,student_borrowed_number,student_limit,student_account,student_is_arrears,student_create_time from card where student_account='1022180429';

-- 修改借阅证信息（仅能修改部分信息）
update card set student_phone='18921965023' where library_card_id=100008;

# 借阅记录信息
-----------

-- 新建借阅记录
insert into record (record_book_ibsn,record_book_title,record_book_author,record_book_publisher,record_book_price,record_book_address,record_library_card,record_student_name,record_student_phone,record_create_time,record_update_time) values('9787111407010','算法导论（原书第3版）','Clifford Stein','机械工业出版社',128,'B2-003',100003,'timwlines','18264552032','1595294570524','1595294570524')

-- 查询借阅记录信息
select record_id,record_book_ibsn,record_book_title,record_book_author,record_book_publisher,record_book_price,record_book_address,record_library_card,record_student_name,record_student_phone,record_state,record_create_time from record where record_library_card='100000';

-- 修改借阅记录（还书时，修改状态）
update record set record_state='1' where record_id=1000000001;

# 系统用户信息（仅有学生自己的用户信息）
-----------

-- 查询用户信息
select user_id,user_email,user_role from user where user_id='1033220427' and user_role=0;

-- 修改自己的密码
update user set user_password='4673c6b7d746904a7bf6c30ac11c8ce5' where user_id='1022180426';
