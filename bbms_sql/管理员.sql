---------------
## 管理员API实现
---------------

# 图书信息
-----------

-- 图书信息列表
select book_ibsn_id,book_title,book_author,book_gist,book_img,book_page,book_price,book_publisher,book_pubdate,book_address,book_now_number,book_number,book_create_time from book where book_is_deleted=0;

-- 查询图书信息
select book_ibsn_id,book_title,book_author,book_gist,book_img,book_page,book_price,book_publisher,book_pubdate,book_address,book_now_number,book_number,book_create_time from book where book_ibsn_id='9787111407010' and book_is_deleted=0;

-- 修改图书信息
update book set book_address='B2-008' where book_ibsn_id=9787121232930;

-- 删除图书信息（软删除）
update book set book_is_deleted='1' where book_ibsn_id=9787115396860;

# 借阅证信息
-----------

-- 借阅证信息列表
select library_card_id,student_name,student_department,student_major,student_phone,student_borrowed_number,student_limit,student_account,student_is_arrears,student_create_time from card where student_is_delete=0;

-- 查询借阅证信息
select library_card_id,student_name,student_department,student_major,student_phone,student_borrowed_number,student_limit,student_account,student_is_arrears,student_create_time from card where student_account='1033180429' and student_is_delete=0;

-- 新增借阅证信息
insert into card (student_name,student_department,student_major,student_phone,student_limit,student_account,student_create_time,student_update_time) values('小明','三个学院','生化','18264526452',20,'1022180427','1595259401380','1595259401380');

-- 修改借阅证信息
update card set student_name='小路' where library_card_id=100009;

-- 删除借阅证信息（软删除）
update card set student_is_delete='1' where library_card_id=100009;

# 借阅记录信息
-----------

-- 借阅记录信息列表
select record_id,record_book_ibsn,record_book_title,record_book_author,record_book_publisher,record_book_price,record_book_address,record_library_card,record_student_name,record_student_phone,record_state,record_create_time from record where record_is_delete=0;

-- 查询借阅记录信息
select record_id,record_book_ibsn,record_book_title,record_book_author,record_book_publisher,record_book_price,record_book_address,record_library_card,record_student_name,record_student_phone,record_state,record_create_time from record where record_id='1000000000' and record_is_delete=0;

-- 修改借阅记录
update record set record_state='2' where record_id=1000000001;

-- 删除借阅记录信息（软删除）
update record set record_is_delete='1' where record_id=1000000001;

# 系统用户信息（仅有学生的用户信息）
-----------

-- 用户信息列表
select user_id,user_email,user_role,user_create_time from user where user_role<1;

-- 查询用户信息
select user_id,user_email,user_role,user_create_time from user where user_id='1033180427' and user_role<1;

-- 新增用户信息
insert into user (user_id,user_password,user_email,user_role,user_create_time,user_update_time) values('1032280425','995e3c00ffab1f547731215f364c7e4d','1033180425@qq.com','0','1595256
077152','1595256077152');

-- 修改用户信息
update user set user_password='995e3c00ffab1f547731215f364c7e4d' where user_id='1022180426';

-- 删除借用户信息（软删除）
update user set user_is_deleted='1' where user_id='1022180426';
