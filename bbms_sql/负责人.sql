---------------
## 负责人API实现
---------------

# 图书信息
-----------

-- 图书信息列表
select * from book;

-- 查询图书信息
select * from book where book_ibsn_id='9787111407010';

-- 新增图书信息
insert into book (book_ibsn_id,book_title,book_author,book_gist,book_img,book_page,book_price,book_publisher,book_pubdate,book_address,book_now_number,book_number,book_create_time,book_update_time) values('9787115508652', '重构', '马丁·福勒', '本书是经典著作《重构》出版20年后的更新版。书中清晰揭示了重构的过程，解释了重构的原理和实践方式，并给出了何时以及何地应该开始挖掘代码以求改善。书中给出了60多个可行的重构，每个重构都介绍了一种经过验证的代码变换手法的动机和技术。本书提出的重构准则将帮助开发人员一次一小步地修改代码，从而减少了开发过程中的风险。 本书适合软件开发人员、项目管理人员等阅读，也可作为高等院校计算机及相关专业师生的参考读物。', 'http://open.liupai.net/mpic/m69470b0cc3394c93bc2bb489fa999266.jpg', 0, 99.00, '人民邮电出版社', '2019-05','B2-011', 4, 4, '1595257418158', '1595257418158');

-- 修改图书信息
update book set book_address='B2-007' where book_ibsn_id=9787121181184;

-- 删除图书信息
delete from book where book_ibsn_id=9787115396860;

# 借阅证信息
-----------

-- 借阅证信息列表
select * from card;

-- 查询借阅证信息
select * from card where student_account='1032280429';

-- 新增借阅证信息
insert into card (student_name,student_department,student_major,student_phone,student_limit,student_account,student_create_time,student_update_time) values('timwlines','物联网工程学院','物联网工程','18262252032',20,'1022180428','1595259320374','1595259320374');

-- 修改借阅证信息
update card set student_name='小李' where library_card_id=100008;

-- 删除借阅证信息
delete from card where library_card_id='100009';

# 借阅记录信息
-----------

-- 借阅记录信息列表
select * from record;

-- 查询借阅记录信息
select * from record where record_id='1000000000';

-- 修改借阅记录
update record set record_state='2' where record_id=1000000001;

-- 删除借阅记录信息
delete from record where record_id='1000000001';

# 系统用户信息
-----------

-- 用户信息列表
select * from user;

-- 查询用户信息
select * from user where user_id='1022180428';

-- 新增用户信息
insert into user (user_id,user_password,user_email,user_role,user_create_time,user_update_time) values('1022180426','995e3c00ffab1f547731215f364c7e4d','1033180426@qq.com','0','1595255
993136','1595255993136');

-- 修改用户信息
update user set user_email='762245831@qq.com' where user_id='1022180428';

-- 删除用户信息
delete from user where user_id='1022180430';
