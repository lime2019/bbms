# bbms_web

## 项目运行

```bash
# 安装依赖
npm install
# 运行
npm run serve
# 打包
npm run build
```

## 项目中插件使用

```bash
# 阿里镜像地址，偶尔可能会出现奇怪的问题，不太推荐使用
--registry=https://registry.npm.taobao.org
# 安装element-ui，桌面组件库
npm i element-ui -S
# 安装axios，基于promise的http库
npm install axios
# 安装nprogress，进度条组件
npm install --save nprogress
# 安装Echart
npm install echarts --save
```
