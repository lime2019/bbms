import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入ElementUI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 引入ECharts及Vue-ECharts
import ECharts from 'echarts'
import VueECharts from 'vue-echarts'
// 设置反向代理，前端请求默认发送到 http://localhost:5000/api
const axios = require('axios')
axios.defaults.baseURL = '/api'
// 全局注册，之后可在其他组件中通过 this.$axios 发送数据
Vue.prototype.$axios = axios
Vue.config.productionTip = false
//全局注册echarts
Vue.prototype.$echarts = ECharts
Vue.component('v-chart',VueECharts)

// 全局使用ElementUI
Vue.use(ElementUI)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
