import Vue from 'vue'
import VueRouter from 'vue-router'
import SearchBook from "../views/Search/SearchBook"
// 引入NProgress进度条
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'SearchBook',
    component: SearchBook
  },{
    path: '/headinghome',
    // name:'HeadingHome',
    // component: HeadingHome,
    component: () => import(/* webpackChunkName: "headinghome" */ '../views/Heading/HeadingHome'),
    children:[
      {
        // path:'charts',
        path:'',
        name:'HeadingsCharts',
        component:()=>import(/* webpackChunkName: "headingcharts" */ '../views/Heading/HeadingCharts')
      }, {
        path:'books',
        name:'HeadingBooks',
        component:()=>import(/* webpackChunkName: "headingbooks" */ '../views/Heading/HeadingBooks')
      },{
        path:'cards',
        name:'HeadingCards',
        component:()=>import(/* webpackChunkName: "headingcards" */ '../views/Heading/HeadingCards')
      },{
        path:'records',
        name:'HeadingRecords',
        component:()=>import(/* webpackChunkName: "headingrecords" */ '../views/Heading/HeadingRecords')
      },{
        path:'users',
        name:'HeadingUsers',
        component:()=>import(/* webpackChunkName: "headingusers" */ '../views/Heading/HeadingUsers')
      }
    ]
  },{
    path: '/adminhome',
    // name: 'AdminHome',
    component: () => import(/* webpackChunkName: "adminhome" */ '../views/Admin/AdminHome'),
    children:[
        {
          path:'',
          name:'AdminBooks',
          component:()=>import(/* webpackChunkName: "adminbooks" */ '../views/Admin/AdminBooks')
        },{
          path:'cards',
          name:'AdminCards',
          component:()=>import(/* webpackChunkName: "admincards" */ '../views/Admin/AdminCards')
        },{
          path:'records',
          name:'AdminRecords',
          component:()=>import(/* webpackChunkName: "adminrecords" */ '../views/Admin/AdminRecords')
        },{
          path:'users',
          name:'AdminUsers',
          component:()=>import(/* webpackChunkName: "adminusers" */ '../views/Admin/AdminUsers')
        }
    ]
  },{
    path: '/studenthome',
    // name: 'StudentHome',
    component: () => import(/* webpackChunkName: "studenthome" */ '../views/Student/StudentHome'),
    children: [
      {
        path:'',
        name:'StudentBooks',
        component:()=>import(/* webpackChunkName: "studentbooks" */ '../views/Student/StudentBooks')
      },{
        path:'records',
        name:'StudentRecords',
        component:()=>import(/* webpackChunkName: "studentrecords" */ '../views/Student/StudentRecords')
      },{
        path:'information',
        name:'StudentInformation',
        component:()=>import(/* webpackChunkName: "studentinformation" */ '../views/Student/StudentInformation')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

// 全局使用NProgress
router.beforeEach((to,from,next)=>{
  NProgress.start()
  next()
})
router.afterEach(()=>{
  NProgress.done()
})

export default router
