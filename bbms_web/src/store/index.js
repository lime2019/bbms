import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loginState:false,
    user:{
      user_id:'',
      user_email:'',
      user_role:-1
    },
    card:{
      library_card_id:'',
      student_name:'',
      student_department:'',
      student_major:'',
      student_phone:'',
      student_borrowed_number:0,
      student_limit:20,
      student_account:'',
      student_is_arrears:1,
      student_create_time:''
    }
  },
  mutations: {
    // 登录
    login(state,payload){
      state.loginState = true
      state.user.user_id = payload.user_id
      state.user.user_email = payload.user_email
      state.user.user_role = payload.user_role
    },
    card(state,payload){
      state.card.library_card_id = payload.library_card_id
      state.card.student_name = payload.student_name
      state.card.student_department = payload.student_department
      state.card.student_major = payload.student_major
      state.card.student_phone = payload.student_phone
      state.card.student_borrowed_number = payload.student_borrowed_number
      state.card.student_limit = payload.student_limit
      state.card.student_account = payload.student_account
      state.card.student_is_arrears = payload.student_is_arrears
      state.card.student_create_time = payload.student_create_time
    },
    // 注销
    logout(state){
      state.loginState = false
      state.user = {
        user_id:'',
        user_email:'',
        user_role:-1
      }
      state.card = {
        library_card_id:'',
        student_name:'',
        student_department:'',
        student_major:'',
        student_phone:'',
        student_borrowed_number:0,
        student_limit:20,
        student_account:'',
        student_is_arrears:1,
        student_create_time:''
      }
    }
  },
  actions: {
  },
  modules: {
  }
})
