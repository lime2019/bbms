# 图书借阅管理子系统API接口文档

## 1. API V1 接口说明

- 接口基准地址：`http://localhost:5000/api`；
- 使用HTTP Status Code 标识状态；
- 数据返回格式统一使用JSON；

#### 1.1. 支持请求的方法

- GET（SELECT）：从服务器中获取资源；
- POST（CREATE）：在服务器新建一个资源；

#### 1.2. 通用返回状态说明

| *状态码* | *含义*                 | *说明*                                                    |
| --- | --- | --- |
| 200      | OK                    | 请求成功                                                   |
| 201      | CREATED               | 创建成功                                                   |
| 204      | DELETED               | 删除成功                                                   |
| 400      | BAD REQUEST           | 请求的地址不存在或者包含不支持的参数                         |
| 401      | UNAUTHORIZED          | 未登录，未授权                                              |
| 403      | FORBIDDEN             | 权限不足，被禁止访问                                        |
| 404      | NOT FOUND             | 请求的资源不存在                                            |
| 422      | Unprocesable entity   | 外键约束问题，[POST] 当修改，删除一个对象时，发生一个验证错误  |
| 500      | INTERNAL SERVER ERROR | 服务器内部错误                                              |


---


## 2. 登录模块

#### 2.1 登录验证接口

- 请求路径：`/login`
- 请求方法：POST
- 请求参数：
  | 参数名 | 参数说明 | 备注 |
  | -- | -- | -- |
  | username | 用户名 | 不能为空 |
  | password | 密码 | 不能为空 |
- 响应参数：
  | 参数名 | 参数说明 | 备注 |
  | -- | -- | -- |
  | user_id | 用户id |  |
  | user_email | 用户邮箱 |  |
  | user_role | 用户角色 |  |
- 响应数据：

  ```javascript
  {
      "msg": {
          "user_id": "1033180429",
          "user_email": "lime2019@qq.com",
          "user_role": 2
      },
      "code": 200
  }
  {
      "data": {},
      "code":401
  }
  ```


## 3. 系统用户信息模块

请求路径：`/users`

#### 3.1 系统用户列表

- 请求路径：`/list`
- 请求方法：GET
- 请求参数：
- 响应参数：
- 响应数据：

                    switch (res.data.code) {
                        case 200:{
                            this.$message({
                                message: '',
                                type: 'success'
                            })
                            break;
                        }
                        case 401:{
                          
                            this.$message({
                                message: '权限不足!',
                                type: 'warning'
                            })
                            break;
                        }
                        case 403:{
                            break;
                        }
                        case 422:{
                            break;
                        }
                        case 500:{
                            this.$message.error('权限不足！');
                            break;
                        }
                        default:{
                            
                        }